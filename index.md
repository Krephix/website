---
layout: social
title: Home
permalink: /
---
## What is Achievement Hunting?

![](/images/server_banner_without_logo.png)

Achievements are rewards put into games by developers for completing various challenges/milestones - they don’t always award direct in-game benefits, but they are part of a platform meta-goal on your gamer profile across all games.

Achievement Hunting is many things, but most of all, achievement hunting is a community of like-minded gamers ([with a lot of unique hunter-slang](/glossary)) celebrating each other’s accomplishments and helping each other to 100% complete the games they love. Achievement Hunting is a goal-oriented way of enjoying video games to 100% while tracking your stats across various metrics on "[achievement trackers](/resources)". An achievement hunter can be someone who either competes on the leaderboards or tracks their stats for any of the following:

* 100%ing as many games as possible [![CME][icon-cme]](https://completionist.me/steam/profiles?display=list&order=desc&sort=completed&search=) [![MGS][icon-mgs]](https://metagamerscore.com/most_score_completist) [![SH][icon-sh]](https://steamhunters.com/profiles?sort=perfect)
* 100%ing very difficult games & achievements [![SH][icon-sh]](https://steamhunters.com/games?sort=worth)
* Chasing the rarest/most “valuable” achievements [![MGS][icon-mgs]](https://metagamerscore.com/all_achievements?sort=ach_value) [![SH][icon-sh]](https://steamhunters.com/profiles?sort=points)
* 100%ing games as fast as possible [![Speedrun][icon-speedrun]](https://www.speedrun.com/) [![SH][icon-sh]](https://steamhunters.com/profiles?sort=fastest)
* Unlocking as many achievements as possible [![CME][icon-cme]](https://completionist.me/steam/profiles?display=list&order=desc&sort=achievements&search=) [![SH][icon-sh]](https://steamhunters.com/profiles?sort=achievements)
* 100%ing games while maintaining a nearly-perfect completion percentage [![CME][icon-cme]](https://completionist.me/steam/profiles?display=list&order=desc&sort=completion&search=) [![SH][icon-sh]](https://steamhunters.com/profiles?sort=avg)

Many achievement hunters feel an awesome sense of completion when they destroy challenging achievements or add another 100% to their list. We all have huge backlogs of games we “will play some day” or “almost finished” so why not start 100%ing them one by one and get your money’s worth?

> “We want games with fun, unique, meaningful, interesting achievements that
> reward us for our skill and offer us extended replayability by demanding
> we play the game in a restricted or strange fashion. We want achievements
> that guide us towards hidden content and easter eggs, achievements that
> require out-of-the-box thinking and problem solving.”
> -- Xeinok (Founder of 100Pals) ([Kotaku Article](https://steamed.kotaku.com/achievement-spam-games-are-causing-controversy-on-steam-1796528445))

Achievement Hunting can be as chill or as competitive as you would like it to be, but there are some [rules](/rules) that all achievement hunters participating in the community and on the leaderboards are expected to follow in order for it to be equal to all hunters. Achievement Hunting is a BIG community. From the achievement trackers' data, there are ~350,000 Steam hunter profiles, ~400,000 Xbox hunters, and ~250,000 on PlayStation.

## Achievement Hunting Discord [![Discord][discord]](https://discord.gg/100Pals)

[![A mosaic of some of the thousands of achievement hunters on the 100Pals Discord][mosiac]](https://discord.gg/100Pals)

100Pals is the home base for all achievement hunters - whether you’re just starting to scratch your completionist itch with a few 100%s or you’re a hardcore dedicated achievement hunter with tens of thousands of achievements under your belt - you are welcome to join the 100Pals Achievement Hunting Community.

The 100Pals Achievement Hunting Discord has thousands of the world's best hunters and is the most active and cutting-edge community for achievement hunters. 100Pals is the only Official Discord Partner Server for Achievement Hunters. Over 500-900+ concurrent hunters online at all times. All members verified.

100Pals was founded in 2015 with the goal of being the central place for achievement hunters; a community where all members are verified to be cheat-free, where toxicity/hate speech is moderated by an active staff, and where hunters are given the tools to discuss achievements, have fun, and coop tons of games to 100%. The 100Pals community very quickly became the most active and largest verified hub for achievement hunters, achievement tracker developers, and the latest tools/projects relating to achievements. 100Pals was selected to be one of the communities represented in the very first wave of [Official Discord Partner Servers](https://discordapp.com/partners) because of its commitment to unification of the achievement hunting community, active moderation, and progressive usage of the Discord community features and bots.

Every day, hunters from every continent (okay… maybe not Antarctica :P), and hundreds of countries around the world - all come together on 100Pals to post their latest 100%s, to discuss the latest strategies, to play games and chat together, and to work on awesome projects ([like verifying and curating the achievements for EVERY game on Steam](https://store.steampowered.com/curator/31507748-Achievement-Scouts/)) ([or like making an awesome mosaic of everyone’s favorite achievements](https://completionist.me/place/100Pals)).

100Pals is officially partnered with: 

* ![][icon-cme] [Completionist.me](https://completionist.me/)
* ![][icon-mgs] [MetaGamerScore](https://metagamerscore.com/)
* ![][icon-sh] [Steam Hunters](https://steamhunters.com/)
* ![][icon-stadiahunters] [Stadia Hunters](https://stadiahunters.com/)
* ![][icon-exo] [Exophase](https://www.exophase.com/)
* ![][icon-cdc] [Caçadores de Conquistas (Portuguese)](https://steamcommunity.com/groups/cacadoresdeconquistas)
* ![][icon-cds] [Chasseurs de Succes (French)](https://steamcommunity.com/groups/100-cds)
* ![][icon-cle] [Caza Logros Españoles (Spanish)](https://steamcommunity.com/groups/cazalogros)

[icon-cme]: /images/content/cme_16x16.png "Completionist.me"
[icon-mgs]: /images/content/mgs_16x16.png "MetaGamerScore"
[icon-sh]: /images/content/sh_16x16.png "SteamHunters"
[icon-stadiahunters]: /images/content/stadiahunters_16x16.png "Stadia Hunters"
[icon-exo]: /images/content/exo_16x16.png "Exophase"
[icon-speedrun]: /images/content/speedrun_16x16.png "speedrun.com"
[icon-cdc]: /images/content/cdc_16x16.png "Caçadores de Conquistas (Portuguese)"
[icon-cds]: /images/content/cds_16x16.png "Chasseurs de Succes (French)"
[icon-cle]: /images/content/cle_16x16.png "Caza Logros Españoles (Spanish)"
[discord]: https://discordapp.com/api/guilds/71459914232365056/widget.png "Discord"
[mosiac]: /images/100Pals_Member_Mosaic_Discord_Logo_Hue.png "A mosaic of some of the thousands of achievement hunters on the 100Pals Discord"
