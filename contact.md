---
layout: default
title: Contact
permalink: /contact
---
# Contact 100Pals Staff

For Media Inquiries, Questions, or Feedback - please feel free to contact us at any of the below.

## Discord

[![Join us on Discord!](https://discordapp.com/api/guilds/71459914232365056/widget.png)](https://discord.gg/100Pals)

## Twitter

<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-mention-button twitter-mention-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 120px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.e3a0e1b01ae601b6c9cf798a93ab7e69.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=https%3A%2F%2Fachievementhunting.com%2Fcontact&amp;screen_name=Xeinok&amp;size=m&amp;time=1570406111559&amp;type=mention" data-screen-name="Xeinok"></iframe>

<iframe id="twitter-widget-1" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-mention-button twitter-mention-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 160px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.e3a0e1b01ae601b6c9cf798a93ab7e69.en.html#dnt=false&amp;id=twitter-widget-1&amp;lang=en&amp;original_referer=https%3A%2F%2Fachievementhunting.com%2Fcontact&amp;screen_name=100PalsOfficial&amp;size=m&amp;time=1570406111560&amp;type=mention" data-screen-name="100PalsOfficial"></iframe>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Email

<staff@100Pals.co>
