#!/usr/bin/env python3
from http.server import HTTPServer, SimpleHTTPRequestHandler
import urllib

class myhandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        print(self.path)
        if self.path.endswith("/"):
            super().do_GET()
            return
        if "." not in self.path:
            newpage = f"{self.path}.html"
            print(f">> {newpage}")
            self.send_response(302)
            self.send_header('Location', newpage)
            self.end_headers()
            return
        super().do_GET()

if __name__ == "__main__":
    server = '127.0.0.1'
    port = 8000
    httpd = HTTPServer((server, port), myhandler)
    print(f"Loading http server on http://{server}:{port}/")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

    httpd.server_close()
    print("Exiting.")