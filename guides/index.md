---
layout: default
title: Guides
permalink: /guides/
---

As pals, we've written a handful of guides on handling a variety of common tasks in the hobby. You can find links to them below.

## Gaming Guides

* [How to savescum](/guides/savescum)
* [How to set up GameAssistant for Achievement Hunters](/guides/gameassistant)
* [How to set up multiple steam accounts for quick local family sharing](/guides/multiple_steam_accounts)
* [How to set up AHK and first steps](/guides/ahk)
* [How to create your own autoclicker](/guides/autoclicker)
* [How to depot a game with depotdownloader](https://steamcommunity.com/sharedfiles/filedetails/?id=2353930763)

## Non-Gaming Guides

* [How to keep your steam account secure](/guides/security)
* [How to make a Merge Request for editing this site or the Unified Rules](/guides/make_a_mr)
